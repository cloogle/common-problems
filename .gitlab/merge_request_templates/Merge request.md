<!-- Thanks for your PR! Please briefly describe the changes: -->

...

<!-- Checklist (change `[ ]` to `[x]` to mark as done): -->

- [ ] The list in `README.md` is updated if a new problem has been added, a problem title has been modified or a file has been renamed.
- [ ] If the list is updated, it is still alphabetically sorted.
- [ ] The added/modified problem description has the `## Solutions` and `## Examples` headings.
- [ ] The added/modified problem description has examples or contains `N/A` under the `Examples` heading.
