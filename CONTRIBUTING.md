# Contributing

To add problems, examples, solutions or edit existing ones, fork this
repository, make your changes and create a pull request. If you don't know the
solution, you can
[create an issue](https://gitlab.com/cloogle/common-problems/issues/new).
Please include a minimal example where the error occurs.

If you make a pull request, please adhere to our format. This ensures the files
are correctly indexed by [Cloogle][]. You can use the existing files for
examples and run `build_index.py` to check for common mistakes.

The list in the [readme](/README.md) contains links to markdown files in the
repository. **Don't forget to update the list.** It is sorted alphabetically.

## Format

The markdown files should look as below. A detailed description follows.

```markdown
# <Some descriptive title (usually a general form of the error you encounter)>

Authors: <authors>

<Some description>

## Solutions

- <solution 1>
- <solution 2>
- ...

## Examples

<example 1>

---

<example 2>

---

...
```

Be minimal in your use of Markdown. [Cloogle][] does not render Markdown fully.
It is capable of rendering `` `code` `` and multiline code blocks (see under
'Examples'), but don't use extra headings, italics, bold text, etc.

### Title

- When writing one description for multiple problems, add multiple titles as
  separate lines.

### Authors

- A list of copyright holders for attribution purposes.
- Names should be real names, not usernames or nicknames.
- Names are comma-separated and we use the Oxford comma: "Alice Doe, Bob Doe,
  and Charlie Doe".

### Solutions

- The `## Solutions` header is required.
- If solutions are not applicable, use `N/A`, although this is only allowed in
  the `compiler-messages` directory.
- Continue solutions with a double space on the next line.

### Examples

- The `## Examples` header is required.
- If examples are not applicable, use `N/A`.
- Put code examples between `` ```clean `` and `` ``` ``.
- Put long monospaced text between `` ```text `` and `` ``` ``.

[Cloogle]: https://cloogle.org
