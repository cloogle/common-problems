# Common problems with Clean programs

This is a list of problems commonly encountered with [Clean][] programs. You
can search in this list or search for errors and faults on [Cloogle][].

Please [contribute](/CONTRIBUTING.md) by adding more examples and errors.
If you don't know the solution yet, you can create a stub page.

All text in this repository is licensed under [CC-BY-SA-4.0](/LICENSE).

### Compile-time errors

- [!= undefined](/compiler-errors/!=-undefined.md)
- [_SystemArray not imported (needed for array denotations)](/compiler-errors/_SystemArray-not-imported.md)
- [_SystemStrictLists not imported (needed for strict lists)](/compiler-errors/_SystemStrictLists-not-imported.md)
- [ambiguous selector specified](/compiler-errors/record-disambiguation.md)
- [application not allowed](/compiler-errors/application-not-allowed.md)
- [attribute variable of lifted argument appears in derived attribute inequality](/compiler-errors/attribute-variable-of-lifted-argument-appears-in-derived-attribute-inequality.md)
- [attribute variable of lifted argument appears in the specified type](/compiler-errors/attribute-variable-of-lifted-argument-appears-in-the-specified-type.md)
- [B, C, F, I, P or R expected](/compiler-errors/B-C-F-I-P-or-R-expected.md)
- [cannot derive a generic instance for an abstract type](/compiler-errors/cannot-derive-a-generic-instance-for-an-abstract-type.md)
- [cannot specialize because there is no instance of](/compiler-errors/cannot-specialize-because-there-is-no-instance-of.md)
- [cannot specialize to this type](/compiler-errors/cannot-specialize-to-this-type.md)
- [cannot unify types](/compiler-errors/cannot-unify-types.md)
- [can't start the clean compiler](/compiler-errors/cant-start-the-clean-compiler.md)
- [Compiler crashed](/compiler-errors/compiler-crashed.md)
- [conflicting kinds](/compiler-errors/conflicting-kinds.md)
- [conflicting priorities](/compiler-errors/conflicting-priorities.md)
- [constructor arguments are missing](/compiler-errors/constructor-arguments-are-missing.md)
- [context restriction not allowed for fully polymorph instance](/compiler-errors/context-restriction-not-allowed-for-fully-polymorph-instance.md)
- [Could not determine the type of this record](/compiler-errors/record-disambiguation.md)
- [defined with wrong arity](/compiler-errors/defined-with-wrong-arity.md)
- [definition in the impl module conflicts with the def module](/compiler-errors/definition-in-the-impl-module-conflicts-with-the-def-module.md)
- [derived type conflicts with specified type](/compiler-errors/derived-type-conflicts-with-specified-type.md)
- [different/incorrect number of members specified](/compiler-errors/different-incorrect-number-of-members-specified.md)
- [duplicate symbol __start](/compiler-errors/duplicate-symbol-__start.md)
- [existential type variable appears in the derived type specification](/compiler-errors/existential-type-variable-appears-in-the-derived-type-specification.md)
- [<expression> expected instead of !](/compiler-errors/expression-expected-instead-of-!.md)
- [false undefined](/compiler-errors/false-undefined.md)
- [first (second) argument of infix operator missing](/compiler-errors/first-second-argument-of-infix-operator-missing.md)
- [function body expected](/compiler-errors/function-body-expected.md)
- [identifier undefined](/compiler-errors/identifier-undefined.md)
- [incompatible patterns in case](/compiler-errors/incompatible-patterns-in-case.md)
- [incompatible types of patterns](/compiler-errors/incompatible-types-of-patterns.md)
- [inconsistent attribution of type definition](/compiler-errors/inconsistent-attribution-of-type-definition.md)
- [inconsistently attributed](/compiler-errors/inconsistently-attributed.md)
- [incorrect module header](/compiler-errors/incorrect-module-header.md)
- [instance cannot be unboxed](/compiler-errors/instance-cannot-be-unboxed.md)
- [internal overloading could not be solved](/compiler-errors/internal-overloading-could-not-be-solved.md)
- [Linker error: could not create .exe](/compiler-errors/Linker-error-could-not-create-exe.md)
- [List brackets, [ and ], around charlist '...' expected](/compiler-errors/List-brackets-around-charlist-expected.md)
- [missing kind argument](/compiler-errors/missing-kind-argument.md)
- [module name does not match file name](/compiler-errors/module-name-does-not-match-file-name.md)
- [multiply defined](/compiler-errors/multiply-defined.md)
- [no generic instances of X for kind K](/compiler-errors/no-generic-instances.md)
- [not defined as a record field](/compiler-errors/not-defined-as-a-record-field.md)
- [Overloading error no instance available of type](/compiler-errors/overloading-error-no-instance-available-of-type.md)
- [Parse error](/compiler-errors/parse-error.md)
- [(pattern variable) already defined](/compiler-errors/pattern-variable-already-defined.md)
- [result type of dynamic must be non-unique](/compiler-errors/result-type-of-dynamic-must-be-non-unique.md)
- [Scanner error: newline in char list](/compiler-errors/Scanner-error-newline-in-char-list.md)
- [Scanner error: newline in string denotation](/compiler-errors/Scanner-error-newline-in-string-denotation.md)
- [selector not defined](/compiler-errors/selector-not-defined.md)
- [specified member type is incorrect](/compiler-errors/specified-member-type-is-incorrect.md)
- [Start has not been declared](/compiler-errors/Start-has-not-been-declared.md)
- [Start rule cannot be overloaded](/compiler-errors/overloading-Start.md)
- [StdEnum not imported (needed for [..] expressions)](/compiler-errors/StdEnum-not-imported.md)
- [This alternative has X arguments instead of Y.](/compiler-errors/This-alternative-has-X-arguments-instead-of-Y.md)
- [true undefined](/compiler-errors/true-undefined.md)
- [type context should contain one or more type variables](/compiler-errors/type-context-should-contain-one-or-more-type-variables.md)
- [type variable of type of lifted argument appears in the specified type](/compiler-errors/type-variable-of-type-of-lifted-argument-appears-in-the-specified-type.md)
- [Unable to get short path name](/compiler-errors/short-path-name.md)
- [undefined in implementation module](/compiler-errors/undefined-in-implementation-module.md)
- [uniqueness specification of instance conflicts with current application](/compiler-errors/uniqueness-specification-of-instance-conflicts-with-current-application.md)
- [universally quantified type variable not allowed here](/compiler-errors/universally-quantified-type-variable-not-allowed-here.md)
- [unused existentially quantified type variable](/compiler-errors/unused-existentially-quantified-type-variable.md)
- [used with too many arguments](/compiler-errors/used-with-too-many-arguments.md)
- [used with wrong arity (constructor)](/compiler-errors/used-with-wrong-arity-constructor.md)
- [used with wrong arity (type)](/compiler-errors/used-with-wrong-arity-type.md)
- [wrongly used or not used at all](/compiler-errors/wrongly-used-or-not-used-at-all.md)
- [X could not be imported](/compiler-errors/X-could-not-be-imported.md)
- [X not imported](/compiler-errors/X-not-imported.md)

### Compile-time warnings

- [alternative will never match](/compiler-warnings/alternative-will-never-match.md)
- [ld: warning: option -s is obsolete](/compiler-warnings/ld-warning-option-s-is-obsolete.md)
- [not all derived strictness information is exported](/compiler-warnings/not-all-derived-strictness-information-is-exported.md)

### Run-time errors

- [Floating point exception](/run-time-errors/floating-point-exception.md)
- [Heap full](/run-time-errors/heap-full.md)
- [index out of range](/run-time-errors/index-out-of-range.md)
- [Not enough memory to allocate heap and stack](/run-time-errors/Not-enough-memory-to-allocate-heap-and-stack.md)
- [Run time error, rule '&#8230;' in module '&#8230;' does not match](/run-time-errors/rule-does-not-match.md)
- [Run time error, selector does not match](/run-time-errors/selector-does-not-match.md)
- [Segmentation fault](/run-time-errors/segmentation-fault.md)
- [Stack overflow](/run-time-errors/stack-overflow.md)

[Clean]: https://clean-lang.org
[Cloogle]: https://cloogle.org
