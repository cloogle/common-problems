# B, C, F, I, P or R expected

Authors: Camil Staps

This error occurs when trying to generate code for a 64-bit ABC file using the
32-bit code generator, or vice versa. The line number refers to the ABC file.

When you have written your own ABC code, it may be incorrect. Otherwise, this
error may indicate a corrupt installation.

## Solutions

- If you wrote your own ABC code, check the `.d` and `.o` directives (see
  below).
- Otherwise, remove `nitrile-packages` and all generated ABC code, fetch the
  dependencies again and rebuild the project.

## Examples

The {{`.d`}} and {{`.o`}} directives describe the number of elements on the A
and B stacks, and the types of the elements on the B stack. Since `Real`s are
64-bit on both 32-bit and 64-bit systems, they take two B-stack spaces on
32-bit systems but only one on 64-bit systems. Hence, the `.d` and `.o`
directives are bitwidth-dependent:

```clean
code {
	.d 0 1 r | 64-bit version
	.d 0 2 r | 32-bit version
}
```

If you depend on this in manually written ABC code, you can use the
`IF_INT_64_OR_32` macro to include one of two alternatives depending on the
bitwidth of the installation.
