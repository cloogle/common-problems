# Linker error: could not create .exe

Authors: Peter Achten and Camil Staps

This error is generated because the compiler wants to build a new executable, but
the old version is still running.

## Solutions

- Terminate the running application and rebuild the new executable.

## Examples

N/A
