# List brackets, [ and ], around charlist '...' expected

Authors: Peter Achten and Camil Staps

This error is caused when writing down `'` `'` and either have zero or more
than one character between `'` and `'` (the character delimiters). In that
case, the compiler expects that you want to create a list of characters instead
of a single character. This requires delimiting the faulty expression with `[`
and `]`.

## Solutions

- If you intended to write down a list of characters, add `[` and `]` around
  the faulty expression.

- If you intended to write down a single character, correct the content between
  `'` and `'`. This can only be a single character, or one of the escape
  characters (`\n`, `\r`, `\f`, `\b`, `\t`, `\\`). For more information, read
  the language report, appendix A.9.

## Examples

N/A
