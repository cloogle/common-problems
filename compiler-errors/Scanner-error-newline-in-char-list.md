# Scanner error: newline in char list

Authors: Peter Achten and Camil Staps

This is most likely caused by using `'` in an identifier (name of a function or
variable). The single quotation mark `'` is used to delimit character constants
(e.g. `'a'`, `'!'`, `'\n'`) and character lists (e.g. `['Hello!']`). You can
use `` ` `` in identifiers.

Another common cause is that you forgot to add a closing `'` in a character constant or
character list.

## Solutions

- Check that your identifiers do not contain `'`, and replace by for instance `` ` ``.

- Check that your character constant is immediately terminated with `'`.

- Check that your character list is terminated with `'`.

## Examples

```clean
my_length :: [a] -> Int
my_length xs = my_length' 0 xs          // local function names often have a simple derived name
where
    my_length' :: !Int [a] -> Int
    my_length' n []       = n
    my_length' n [_ : xs] = my_length' (n+1) xs
```

Fix this problem by renaming the local function:

```clean
my_length :: [a] -> Int
my_length xs = my_length` 0 xs          // local function names often have a simple derived name
where
    my_length` :: !Int [a] -> Int
    my_length` n []       = n
    my_length` n [_ : xs] = my_length` (n+1) xs
```

---

```clean
is_digit :: Char -> Bool
is_digit c = c >= '0' && c <= '9     // forgot to close character constant '9'
```
Fix this problem by adding `'`:

```clean
is_digit :: Char -> Bool
is_digit c = c >= '0' && c <= '9'
```

---

```clean
is_hello :: [Char] -> Bool
is_hello ['hello]  = True             // forgot to close character list ['hello']
is_hello _         = False
```

Fix this problem by inserting `'`:

```clean
is_hello :: [Char] -> Bool
is_hello ['hello'] = True
is_hello _         = False
```
