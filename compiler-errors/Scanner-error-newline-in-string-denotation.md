# Scanner error: newline in string denotation

Authors: Peter Achten and Camil Staps

This is most likely caused by forgetting to close a string constant with `"` in a pattern
or right-hand side expression. If you need to have a newline character in your string, you must
use `\n` for the newline character.

## Solutions

- Check that your string constants always end with `"` on the same line.

## Examples

```clean
is_digit :: String -> Bool
is_digit c = isMember c ["0","1","2","3","4,"5","6","7","8","9"]  // forgot to close "4"
```

Fix this problem by closing string constant `"4"`:

```clean
is_digit :: String -> Bool
is_digit c = isMember c ["0","1","2","3","4","5","6","7","8","9"]
```

---

```clean
is_hello :: String -> Bool
is_hello "hello  = True    // forgot to close "hello"
is_hello _       = False
```
Fix this problem by closing `"hello"`:

```clean
is_hello :: String -> Bool
is_hello "hello" = True
is_hello _       = False
```

---

```clean
is_quotation_mark :: String -> Bool
is_quotation_mark """ = True    // the second " closes the string pattern instead of referring to the " character
is_quotation_mark _   = False
```

Fix this problem by escaping the `"` in a string with `\"`:

```clean
is_quotation_mark :: String -> Bool
is_quotation_mark "\"" = True
is_quotation_mark _    = False
```
