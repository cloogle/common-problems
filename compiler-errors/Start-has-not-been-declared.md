# Start has not been declared

Authors: Peter Achten and Camil Staps

This error occurs when the main module of your project (this is often the module that starts
with the keyword `module`) does not contain the `Start` function. 

## Solutions

- Make sure that the module contains at least one `Start` function.

## Examples

```clean
module test
import StdEnv

start = 42  // function names are case-sensitive, the Start function
            // *must* begin with capital S
```
Here you fix the problem by changing the name of `start` into `Start`.
