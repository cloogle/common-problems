# StdEnum not imported (needed for [..] expressions)

Authors: Peter Achten and Camil Staps

This error occurs when you use syntax constructs which require the functions of
`Enum` and `IncDec` classes defined in `_SystemEnum`. For instance, `[0..]`
requires `_from`.

## Solutions

- Add `import StdEnum` to your module (preferred minimal solution)
- Add `import StdEnv` to your module (also preferred, StdEnv includes StdEnum)
- Add `import _SystemEnum` to your module (works, but not preferred)

## Examples

```clean
module my_module

Start = [0..]
```
