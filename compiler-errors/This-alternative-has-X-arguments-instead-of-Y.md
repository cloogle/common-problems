# This alternative has X arguments instead of Y.

Authors: Peter Achten and Camil Staps

A function definition has a number of arguments (arity). If you provide the
type signature of the function yourself, then the number of arguments in the
type signature determine the arity of the function. If you omit a type
signature, then the number of arguments of the first function alternative
determine the arity of the function. All function alternatives must have the
same arity. The error message tells you which function alternatives have a
different arity. Frequent causes for this error are forgetting to mention
parameters or having forgotten to add brackets (`(` and `)`) around patterns of
algebraic data types.

## Solutions

- Check every function alternative and make sure that its arity is the same as that of its type
  signature or the first function alternative (in the absence of a type signature).

- Check that patterns of algebraic data types are surrounded by brackets (`(` and `)`).

## Examples

```clean
module test

weird_function :: Int Int Int Int -> Int   // function signature has arity 4
weird_function 0 a     = a                 // but this alternative has arity 2
weird_function 1 a b   = b                 // and this alternative has arity 3
weird_function 2 a b c = c                 // this alternative is fine
weird_function x a b c = x                 // and so is this one

Start = 42
```
Compiling this module generates two compiler errors:
```
Error [test.icl,5,weird_function]: This alternative has 3 arguments instead of 4.
Error [test.icl,4,weird_function]: This alternative has 2 arguments instead of 4.
```
You can fix it by giving all alternatives arity 4:
```clean
weird_function :: Int Int Int Int -> Int
weird_function 0 a _ _ = a
weird_function 1 a b _ = b
weird_function 2 a b c = c
weird_function x a b c = x
```

---

```clean
module test

weird_function 0 a     = a    // no type signature, so this alternative determines the arity, which is 2
weird_function 1 a b   = b    // this alternative has arity 3
weird_function 2 a b c = c    // this alternative has arity 4
weird_function x a b c = x    // this alternative has arity 4

Start = 42
```
Compiling this module generates three compiler errors:
```
Error [test.icl,6,weird_function]: This alternative has 4 arguments instead of 2.
Error [test.icl,5,weird_function]: This alternative has 4 arguments instead of 2.
Error [test.icl,4,weird_function]: This alternative has 3 arguments instead of 2.
```
You can fix it by giving all alternatives arity 4:
```clean
weird_function 0 a _ _ = a
weird_function 1 a b _ = b
weird_function 2 a b c = c
weird_function x a b c = x
```

---

```clean
module test
import StdEnv

:: Shape = Circle Real | Rectangle Real Real

width :: Shape -> Real
width Circle r        = 2.0 * r   // forgot to add ( ) around the pattern (Circle r)
width (Rectangle w _) = w
```

Compiling this module generates the compiler error:

```
Error [test.icl,7,width]: This alternative has 2 arguments instead of 1.
```

You can fix it by adding `(` and `)` around the intended pattern:

```clean
width (Circle r)      = 2.0 * r
```
