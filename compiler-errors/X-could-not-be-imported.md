# X could not be imported

Authors: Camil Staps

This error occurs when an imported module could not be found. The module name
should *not* contain the library (e.g. `base-stdenv` or `platform`) and should
*not* contain the extension (`.dcl` or `.icl`). Examples of frequently used
module names are `StdEnv` and `Data.Maybe`.

## Solutions

- Check the spelling of the module name, and make sure it does not contain the
  library name or the file extension.
- Make sure that the right search paths are given in nitrile.yml.

## Examples

N/A
