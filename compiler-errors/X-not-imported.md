# X not imported

Authors: Camil Staps

This error occurs when a module uses qualified imports, and some of the
imported elements require other elements to be imported while they are not.

## Solutions

- Add the relevant elements to the qualified imports.
- Make your qualified imports more selective by not importing all members of a
  class, all constructors of an algebraic data type, or all fields of a record.

## Examples

```clean
from Data.Map import put
```

This results in the errors `class < not imported` and `type Map not imported`,
because the type of `put :: k a (Map k a) -> Map k a | < k` uses these
elements. Add them to the qualified imports:

```clean
from StdOverloaded import class <
from Data.Map import :: Map, put
```

---

```clean
from Data.Foldable import class Foldable(..)
```

Here, `class Monoid` must be imported as well, because `Foldable`'s members
`fold` and `foldMap` use it. However, if we only need the class definition of
`Foldable` and not its members, we can use:

```clean
from Data.Foldable import class Foldable
```

And if only some members, say `foldr` and `foldl`, are needed:

```clean
from Data.Foldable import class Foldable(foldr,foldl)
```

---

```clean
from Data.Func import `on`
```

This results in `function/macro on not imported`, because `` `on` `` is a macro
that uses `on` in its definition. Add it to the qualified imports:

```clean
from Data.Func import on, `on`
```
