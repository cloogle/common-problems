# application not allowed

Authors: Peter Achten

This error occurs when a pattern match is written in the form of a function application.
This is an error because a pattern match can only be used to inspect the structure of a 
value, and never that of a computation. 


## Solutions

- Check if you accidentally omitted a `,` in a tuple or list pattern, and fix it.


## Examples

```clean
g :: (a,b) -> a
g (x y) = x
```
Here, the pattern says `(x y)`, but should be: `(x,y)`.

---

```clean
g :: [a] -> a
g [x y] = x
```
Similarly, the pattern says `[x y]`, but should be: `[x:y]` or `[x,y]`.
