# attribute variable of lifted argument appears in the specified type

Authors: Camil Staps

This error occurs when an attribute variable of a local definition (for
instance, in a `where` block) depends on the type of the function it is a part
of.

## Solutions

- The easiest solution is usually to remove the illegal type definition of the
  local function.

## Examples

```clean
id :: .a -> .a
id x = y
where
	y :: .a
	y = x
```

Here, the type attribute `.` of `y` indicates that it can either be unique or
shared. However, in reality the instantiation of `.` (i.e., whether it is `*`
or not) depends on the instantiation of the `.` variables in the type of `id`.
The error is resolved by removing the type signature of `y`.
