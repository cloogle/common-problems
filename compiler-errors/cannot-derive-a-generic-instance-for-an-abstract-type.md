# cannot derive a generic instance for an abstract type

Authors: Camil Staps

This error occurs when trying to `derive` a generic function for an abstract
type. This is not possible, because the generic representation cannot be built
if the type representation is unknown.

This error also occurs for some predefined types which are considered, such as
`{#}` (for this type, it is notoriously hard to write a general instance).

## Solutions

- Implement the generic function in the module containing the type.
- Export the type.
- Manually implement the generic function.

## Examples

```clean
import Data.GenDefault
derive gDefault {#}
```
