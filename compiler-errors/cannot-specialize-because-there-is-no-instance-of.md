# cannot specialize because there is no instance of

Authors: Mart Lubbers

This error is caused if you want to automatically derive a generic function for
a type and not all required specializations exist.

## Solutions

- Give a specialized implementation for the required specialization.

## Examples

```clean
module test

import StdGeneric

generic gFun a :: a
gFun{|OBJECT|} fx = OBJECT fx
gFun{|CONS|} fx = CONS fx

derive gFun T

:: T = T

Start :: T
Start = gFun{|*|}
```

This causes `cannot specialize because there is no instance of UNIT`. To fix it
we give an implementation for `UNIT`:

```clean
module test

import StdGeneric

generic gFun a :: a
gFun{|OBJECT|} fx = OBJECT fx
gFun{|CONS|} fx = CONS fx
gFun{|UNIT|} = UNIT

derive gFun T

:: T = T

Start :: T
Start = gFun{|*|}
```
