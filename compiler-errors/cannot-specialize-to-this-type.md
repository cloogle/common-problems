# cannot specialize to this type

Authors: Mart Lubbers

This error is caused if you want to automatically derive a generic function for
a type that does not support automatic derivation.

Examples of such types are: Basic types (excluding `()`), functions, quantified
ADTs and extensible ADTs.

## Solutions

- Give a specialized implementation for the type.

## Examples

```clean
module test

import StdGeneric

generic gFun a :: a
derive gFun Int

Start :: Int
Start = gFun{|*|}
```

To fix this a specialized instance can be given:

```clean
module test

import StdGeneric

generic gFun a :: a
gFun{|Int|} = 42

Start :: Int
Start = gFun{|*|}
```
