# cannot unify types

Authors: Peter Achten and Camil Staps

This error is caused by calling a function (or operator) with (one or more)
argument(s) of the wrong type. Here, unify means that the type of the argument can not be matched
(unified) with the expected type.

## Solutions

- It is hard to give a general solution, because this error can be generated in an astounding number of ways.
  In general, check the line number that is given in the error message to see where the error is detected. 
  The error message displays the two types that can not be unified. This should give you a clue where to look
  in your code. 

## Examples

```clean
sign` :: Real -> Int  // the type signature states that x must be of type Real
sign` x
| x > 0     =  1      // the (>) operator must work on values of the same type,
| x < 0     = -1      // so x is inferred to have type Int here, which conflicts with the type signature
| otherwise =  0
```

Here you fix the problem by changing the first two occurrences of `0` (which is of type `Int`) into
`0.0` (which is of type `Real`).

---

```clean
Start = isMember [1,2,3] 42   // the type of isMember (StdList) is: a [a] -> Bool | Eq a
```

Here the arguments got mixed up, and the solution is to swap them (`isMember 42 [1,2,3]`).

---

```clean
Start = map + [1,2,3]
```

The author intended to write `map (+) [1,2,3]`, but forgot the brackets around `+`, so now the program
attempts to add the function `map` to the list `[1,2,3]`.
Add the missing brackets to fix this problem.
