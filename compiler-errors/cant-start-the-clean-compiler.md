# can't start the clean compiler

Authors: Camil Staps

This error indicates a corrupt installation. It should not normally occur.

## Solutions

- Remove `nitrile-packages` and rerun `nitrile fetch`.
- Report the bug.

## Examples

N/A
