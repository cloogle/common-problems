# Compiler crashed

Authors: Camil Staps

This error should not normally occur and indicates a corrupt installation or a
bug.

## Solutions

- Report the bug.

## Examples

N/A
