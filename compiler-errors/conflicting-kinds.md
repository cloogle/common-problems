# conflicting kinds

Authors: Camil Staps

This error occurs when a type is used to instantiate a type variable with
another kind, or when a type variable is used with different ununifiable kinds.

Imprecisely, the kind of a type refers to whether it is a 'full' type or not.
For instance, `Int`, `Bool` and `[a]` are all 'full' types, in that there can
be values of these types (e.g., `37`, `True` and `[1,2,3]`). But `[]` is not a
'full' type, since there are no values of this type (e.g., `[1]` is of type
`[Int]`, not `[]`).

(More precisely, the kind of a type refers to the number and kind of its
variables, and can be seen as the type of a type.)

Usually, the error indicates that you forgot a type argument (see the first two
examples below), although it can also occur in more complex contexts (see the
last example below).

## Solutions

- Carefully check the function types.

## Examples

```clean
:: T a = T a
f :: T -> T
f x = x
```

Here, `T` is used as a 'full' type (kind `*`, like `Int`, etc.). However, the
type definition is `:: T a = T a`. It has a type variable; its actual kind is
`*->*` and 'full' types using `T` are for example `T Int`. Hence, a correct
type for `f` is `f :: (T a) -> T a` (or, of course, `f :: a -> a` in this
case).

---

```clean
import StdEnv
instance + (?)
where
	(+) (?Just x) (?Just y) = ?Just (x+y)
	(+) _          _        = ?None
```

Here, `(?)` is used to instantiate the variable `a` in
`class + a :: a a -> a`. But from the type of `+` one can derive that `a` must
be a 'full' type (kind `*`) there. `(?)` is not a 'full' type since it has a
type variable. A correct instance header for this instance would be:

```clean
instance + (?a) | + a
```

---

```clean
f :: (a b) -> a
```

Here, from the usage `(a b)` one can derive that `a` is not a 'full' type
(it has kind `k->*` where `b :: k`). But `a` is also the result type of the
function, so it also must be a 'full' type (kind `*`). Hence, there is no `a`
possible that adheres to this definition; the type is meaningless.
