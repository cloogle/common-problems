# conflicting priorities

Authors: Camil Staps

This error occurs when an expression with infix operators can be parsed in
multiple ways.

When an expression with infix operators is parsed, the operators with the
highest priority (e.g. `5` in `infix 5`) bind strongest. So `1 + 2 * 3` is
parsed as `1 + (2 * 3)` because `*` has a higher priority than `+`. In a
sequence with multiple operators of the same priority (e.g. `10 - 5 - 3`), the
associativity of the operators is used. Because `-` is left-associative (it is
defined as `infixl`, not `infixr`), the example above is parsed as
`(10 - 5) - 3`, not `10 - (5 - 3)`.

However, when operators with the same priority do not agree on associativity
(i.e., one is left-associative and the other right-associative, or at least one
of them has no associativity specified), there is no unambiguous way to parse
the expression. This triggers the "conflicting priorities" error.

## Solutions

- Add parentheses to clarify the expression.
- Change the priority and/or associativity of one or more infix operators.

## Examples

```clean
(<~) infixr 5 :: a b -> a
(<~) x y = x

(~>) infixl 5 :: a b -> b
(~>) x y = y

Start = 1 ~> 2 <~ 3
```

Here, `<~` is a right-associative operator while `~>` is left-associative, and
they both have priority `5`. Therefore it is unclear whether the `Start` rule
should be read as `(1 ~> 2) <~ 3` or as `1 ~> (2 <~ 3)`. Clarify this using
parentheses, or change the priorities and/or associativity of the operators.

---

```clean
import StdEnv
Start = 10 mod 5 mod 2
```

The `mod` class is defined with `infix 7`; no associativity is specified. This
makes it ambiguous whether `(10 mod 5) mod 2` or `10 mod (5 mod 2)` is
intended. Include parentheses to disambiguate the expression.
