# constructor arguments are missing

Authors: Peter Achten and Camil Staps

This error is caused when a data constructor that expects at least one argument occurs in a pattern match 
all by itself (so, without any arguments). This can occur at the left hand side of a function definition 
(note that the function definition must not have a type declaration, otherwise you receive an error message
about the incorrect arity), or in a case-expression.

## Solutions

- Identify and add missing arguments of the data constructor and do not forget to add parentheses `(` `)` around 
  the entire pattern match involving the data constructor.

## Examples

```clean
:: U = U Bool   // the data constructor U expects a Bool argument

h U b = b       // this location causes the error, ( ) are missing around (U b)

Start = h (U True)
```

Here you fix the problem by changing the function definition of `h` into: `h (U b) = b`.

---

```clean
:: U = U Bool   // the data constructor U expects a Bool argument

h x = case x of
         U = x  // this location causes the error, U has no arguments

Start = h (U True)
```

Solve the problem by adding the correct number of arguments to `U` in the case-expression.
