# context restriction not allowed for fully polymorph instance

Authors: Camil Staps

This error disallows the definition of fully polymorph class instances (i.e.,
instances on the most general type `a`) with a class restriction (e.g.,
`toString a`).

## Solutions

- In some cases, the context restriction can be moved to the class, as has been
  done for {{`*>`}} and {{`<*`}} in `Control.Applicative`.

## Examples

```clean
module test

import StdEnv

class prettyprint a :: a -> String

instance prettyprint a | toString a where prettyprint x = toString x
instance prettyprint Char where prettyprint c = "'" +++ c +++ "'"
```

Here, the `prettyprint` class is given a default instance that works for any
type implementing `toString`. However, this is not allowed. In some use cases,
this is resolved by moving the context restriction to the class:

```clean
class prettyprint a | toString a :: a -> String

instance prettyprint a    where prettyprint x = toString x
instance prettyprint Char where prettyprint c = "'" +++ c +++ "'"
```

However, this is only allowed by developmental versions of the compiler.
