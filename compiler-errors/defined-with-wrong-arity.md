# defined with wrong arity

Authors: Camil Staps

This error occurs when a class instance has a member with an incorrect number
of arguments (arity).

## Solutions

- Carefully check the number of parameters of the member definitions.

## Examples

```clean
instance toString (a, b) | toString a & toString b
where
	toString x y = toString x +++ ", " +++ toString y
```

Here, `toString x y` suggests that `toString` gets two arguments. Instead, it
gets only one argument (as can be seen from its type:
`class toString a :: !a -> String`), so the member should be defined as:

```clean
	toString (x,y) = toString x +++ ", " +++ toString y
```
