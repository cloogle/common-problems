# definition in the impl module conflicts with the def module

Authors: Peter Achten and Camil Staps

This error occurs when the type signature of an element in module `X.dcl` is different from
the type signature of the same element in module `X.icl`. This is often the case after 
editing stuff in the implementation module. 

## Solutions

- Make the definitions identical in both the corresponding modules `X.dcl` and `X.icl`

## Examples

N/A
