# derived type conflicts with specified type

Authors: Peter Achten and Camil Staps

This error occurs when you have given a type to a function that can not be matched with 
the inferred type (derived type) by the compiler. The good news about this error message
is that your function is internally consistent (otherwise no type could be inferred in 
the first place) and the compiler can show inferred types for you. 

## Solutions
- Remove the type that you specified or put it in a comment and let the
  compiler show the inferred types.

## Examples

```clean
my_fun :: a -> a
my_fun (x,y) = (x,y)
```
Here the specified type `a -> a` is more general than the inferred type `(a,b) -> (a,b)`.

---

```clean
dupl :: a -> (a,b)
dupl x = (x,x)
```
Here the specified type `a -> (a,b)` is incorrect. The inferred type is `a -> (a,a)`.
