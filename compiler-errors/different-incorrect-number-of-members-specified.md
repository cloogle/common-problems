# different/incorrect number of members specified

Authors: Peter Achten and Camil Staps

This error occurs when a class instance defines more or less member functions
than the class has. This can be due to typos in the member names, which leads
the compiler to see different alternatives as different functions.

When the instance is exported, the error message is `incorrect number of
members specified`. Otherwise, it is `different number of members specified`.

## Solutions

- Carefully check the names of the member functions.
- Check the class definition to see if you implemented all member functions.

## Examples

```clean
:: Color = Red | Blue

instance toString Color
where
	toString Red  = "Red"
	tostring Blue = "Blue"
```

Here, there is a typo in the second alternative of the `toString` function: it
is spelled `tostring`, with a lower case `s`. Because of this, the compiler
reads it as two different functions, meaning this instance has two members
instead of one. Resolve the error by fixing the typo.
