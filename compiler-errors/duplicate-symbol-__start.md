# duplicate symbol __start

Authors: Mart Lubbers and Camil Staps

This error is generated when multiple object files are linked together,
containing symbols with the same names, and no optimizing linker is used.

For example, this problem occurs on Linux and Mac systems when you import
modules containing a `Start` rule.

## Solutions

- Remove the `Start` function from the imported module.

## Examples

N/A
