# existential type variable appears in the derived type specification

Authors: Camil Staps

This error occurs when the type of a function depends on the run-time
instantiation of existential type variables.

## Solutions

- Usually, this error indicates a fundamental flaw in the setup of your
  program. It may be that existential typing is wholly unsuitable for your
  application (and perhaps dynamics are more appropriate), or it may be that
  you need to provide helper functions to access the type that contains
  existential type variables.

## Examples

```clean
:: T = E.a: T a

fromT :: T -> a
fromT (T x) = x

Start = fromT (T 5)
```

Here, the type of `fromT :: T -> a` suggests that the function can return *any*
type depending on the context of the caller. That is however not the case, as
the type `a` depends on the instantiation of the existential type variable of
`T`. The type derived for this function is thus something like `T -> (E.a: a)`,
which is not a valid type for functions.

If all that ever needs to happen with the existentially quantified value is,
for instance, transforming it to a string, this functionality can be
internalised:

```clean
:: T = E.a: T a & toString a

instance toString T where toString (T x) = toString x

Start = toString (T 5)
```

It may also be that dynamics are more appropriate for your use case:

```clean
fromDynamic :: Dynamic -> a | TC a
fromDynamic (x :: a^) = x
fromDynamic _         = abort "type error\n"

Start :: Int
Start = fromDynamic (dynamic 5)
```

The exact solution depends on your application.
