# <expression> expected instead of !

Authors: Peter Achten and Camil Staps

You probably used `!` as the boolean negation operator. You must use the
function `not` for this purpose (`!` is reserved and relates to strictness
properties). 

## Solutions

- Replace `!` with `not`, and make sure to add `import StdEnv` in your module.

## Examples

```clean
divide x y
| !(y == 0) = [x / y]   // you intend to use ! as not
| otherwise = []
```
Here you can fix the problem by using `not` instead of `!`:

```clean
divide x y
| not (y == 0) = [x / y]
| otherwise    = []
```

or use the `<>` operator, which is an abbreviation for this pattern:

```clean
divide x y
| y <> 0    = [x / y]
| otherwise = []
```
