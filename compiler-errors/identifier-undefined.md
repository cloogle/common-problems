# identifier undefined

Authors: Camil Staps

You attempted to use a function (class, type, etc.) which is not defined.

## Solutions

- Check the spelling of the name. Names are case sensitive.
- Perhaps the function exists but needs to be imported. If you do not know in
  which module it is defined, you can search for it on Cloogle.

## Examples

```clean
module test

import StdEnv

Start = trace_n "test" 37
```

Here, `trace_n` is defined in `StdDebug` which is not a part of `StdEnv`. It
needs to be imported separately:

```clean
import StdEnv, StdDebug
```

---

```clean
module test

id :: a -> a
id x = x

Start = Id 37
```

Here, `id` was defined but the start rule uses `Id`. Fix the spelling to use
`id` instead.
