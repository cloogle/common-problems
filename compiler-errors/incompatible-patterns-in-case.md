# incompatible patterns in case

Authors: Peter Achten

This error occurs in a function with several alternatives in which
the pattern match on a particular argument (first, second, third, etc) belongs to a different
type than the one found in another alternative. 

## Solutions
- Make sure that within your function the patterns that belong to the same argument belong
  to the same type.

## Examples

```clean
print 0     = "0"
print False = "False"
```

Here, the type derived of the pattern of the first alternative is `Int` and the type 
derived of the pattern of the second alternative is `Bool`. These are not the same. If it 
is your intention to create a function that is to be applied to values of different types, 
you should consider to create an overloaded function.

---

```clean
my_sum (a,b)   = a+b
my_sum (a,b,c) = a+b+c
```

Here, the type derived of the pattern of the first alternative is `(a,a) | + a` and the type
derived of the pattern of the second alternative is `(a,a,a) | + a`. These are not the same.
If it is your intention to create a function that is to be applied to values of different types, 
you should consider to create an overloaded function.
