# incompatible types of patterns

Authors: Peter Achten

This error occurs in a function with a `case` construct that has an alternative 
in which the pattern match belongs to a different type than the one found in another 
alternative of the same `case`. 

## Solutions
- Make sure that within a `case` the patterns belong to the same type.

## Examples

```clean
my_fun x = case x of
	0     = "0"
	False = False"
```

Here, the type derived of the first alternative is `Int` and the type 
derived of the pattern of the second alternative is `Bool`. These are not the same. If it 
is your intention to create a function that is to be applied to values of different types, 
you should consider to create an overloaded function.

---

```clean
my_sum x = case x of
	(a,b)   = a+b
	(a,b,c) = a+b+c
```

Here, the type derived of the pattern of the first alternative is `(a,a) | + a` and the type
derived of the pattern of the second alternative is `(a,a,a) | + a`. These are not the same.
If it is your intention to create a function that is to be applied to values of different types, 
you should consider to create an overloaded function.
