# inconsistent attribution of type definition

Authors: Camil Staps

This error occurs when a type's right-hand side is annotated with uniqueness
attributes, but the type name itself is not correctly annotated. For instance,
when an ADT constructor has unique parameters, the type itself should also be
unique.

## Solutions

- Add the correct annotation (usually `*`) to the type name.

## Examples

```clean
:: T = C *Int
```

Here, `C` gets a unique parameter, which means that `T` is always unique. This
must be reflected in the left-hand side of the definition:

```clean
:: *T = C *Int
```
