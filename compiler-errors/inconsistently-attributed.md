# inconsistently attributed

Authors: Camil Staps

This error occurs when a type variable is used with different attributes (e.g.,
`*`, `.`, `u:`, or empty). This is not allowed by the type system, even though
in some cases the program is not inherently faulty.

## Solutions

- Annotate all occurrences of each type variable with the same annotation.

## Examples

```clean
import StdMisc

f :: a -> .a
f _ = undef
```

The error occurs here because `a` occurs both without attribute (in the
argument) and with a variable uniqueness attribute (in the result). This is not
allowed by the type system, even though there is nothing wrong with the
function in particular (the same function with the more general type `b -> .a`
does compile). The solution is to make sure all occurrences of `a` are
annotated in the same way.
