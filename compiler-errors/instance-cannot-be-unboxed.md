# instance cannot be unboxed

Authors: Camil Staps

This error occurs when an expression requires that values of a certain type can
be unboxed. Only basic types (`Int`, `Bool`, `Real`, unboxed arrays of those
types and fully strict records containing only those types) can be unboxed,
meaning they are stored more efficiently in memory and can be dealt with more
efficiently. Unboxing is needed to construct arrays and lists with the `#`
annotation.

## Solutions

- If practically possible, change the stored type so that it can be unboxed.
- Use a different type of array or list.

## Examples

```clean
Start = [# ?Just 37]
```

Here, we attempt to create a list of type `[#(?Int)]`. Since `(?)` is not a
basic type, this is not possible. Either use a different kind of list (`[?Just
37]`) or don't use `(?)` (for instance, by using `-1` as a `?None` value).
