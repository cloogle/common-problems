# internal overloading could not be solved

Authors: Peter Achten and Camil Staps

There are several ways to create this error. The first is that you have defined an overloaded function
but forgot to include the overloading class constraints in the type signature of the function (see 
example 1 below). The second way is caused by applying overloaded functions in such a way that it can 
not be determined which overloaded function should be chosen (see example 2 below).

## Solutions

- Check that the type signatures of your functions include all overloading class constraints (if any).

- Check that combinations of overloaded functions can always be resolved (i.e.: it is clear which instance
  must be chosen).

## Examples

```clean
add :: (a,a) -> a   // the error is that the class constraint `| + a` is missing
add (x,y) = x + y
```

This is fixed by correcting the type signature to `add :: (a,a) -> a | + a`.

---

```clean
instance toString Color
where
	toString Red  = "Red"
	toString Blue = "Blue"

instance fromString Color
where
    fromString "Red"  = Red
    fromString "Blue" = Blue
    fromString str    = abort ("could not parse '" +++ str +++ "' as Color\n")

parseprint :: String -> String
parseprint x = toString (fromString x)  // the problem occurs in this function
```

The problem is that in `parseprint` you cannot determine which instance of `fromString` to use in 
the expression `(fromString x)`. As a consequence, this is neither possible for the application of 
`toString`. In these situations the solution is to express explicitly in your program which instance
you need. This can be done via a local definition, such as:

```clean
parseprint :: String -> String
parseprint x = toString y
where
	y :: Color  // this type signature selects the Color instance of fromString
	y = fromString x
```
