# missing kind argument

Authors: Camil Staps

This error occurs when a generic is used without its kind argument (i.e.,
`gEq x y` instead of `gEq{|*|} x y`).

Generics are kind-indexed classes, that is, there are different classes
depending on the kind of the type that it is used for. Thus, you could also use
e.g. `gEq{|*->*|} (==) (?Just x) (?Just y)` to use the `==` instance on `x` and
`y` instead of the `gEq` derivation. For this reason, you have to specify the
kind argument.

## Solutions

- Add the kind argument.

## Examples

```clean
module test

import Data.GenDefault

Start :: Int
Start = gDefault
```

The correct Start rule is `gDefault{|*|}`.
