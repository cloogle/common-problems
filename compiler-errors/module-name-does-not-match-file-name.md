# module name does not match file name

Authors: Peter Achten

This error occurs when the name of your (main / implementation / definition) module is not equal 
to the file name without file extension (.icl in case of a main or implementation module, and .dcl 
in case of a definition module). Module names are case sensitive.

## Solutions

- Make sure that the module name and the file name are exactly identical.

## Examples

The file "Test.icl" has this content:

```clean
module test  // this main module name does not start with an uppercase T

Start = 42
```

You can fix this either by renaming the file to `"test.icl"` or rename the module to `module Test`.

---

The file "Test.icl" has this content:

```clean
implementation module test  // this implementation module name does not start with an uppercase T

Start = 42
```

You can fix this either by renaming the file to `"test.icl"` or rename the module to `implementation module Test`.

---

The file "Test.dcl" has this content:

```clean
definition module test  // this definition module name does not start with an uppercase T
```

You can fix this either by renaming the file to `"test.dcl"` or rename the module to `definition module Test`.
