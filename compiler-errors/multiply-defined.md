# multiply defined

Authors: Peter Achten and Camil Staps

This error occurs when the (imported) modules in your project try to define the same 
identifier (e.g. function, operator, type constructor, data constructor) at least twice. 
This can happen when you are merging files from two projects into one project, or you
happened to reuse an existing name from for instance `StdEnv`.

## Solutions

- If you reused an existing name from one of the standard libraries, first check if you
  actually defined the same function. If this is the case, delete your function and use
  the version from the standard library. If this is not the case, then you should rename
  your function in order to avoid confusion.

- If the implementation of a function in your module is scattered throughout
  the file, bring all pieces together (see example below).

## Examples

```clean
module test

square :: !Int -> Int
square 2 = 4

Start = square 2

square 3 = 9
```

In this example without imports, the definition of `square` is interspersed
with the definition of `Start`. Moving the different alternatives of `square`
together resolves the issue:

```clean
square :: !Int -> Int
square 2 = 4
square 3 = 9
```
