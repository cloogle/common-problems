# no generic instances of X for kind K

Authors: Camil Staps

This error is a special case of {{no instance available of type ...}}. It
indicates that a generic function is missing the instances necessary in some of
its usages.

## Solutions

- Add the required instances to the generic function.

## Examples

```clean
generic gFun a :: a

Start :: Int
Start = gFun{|*|}
```

Here, to apply `gFun{|*|}` for `Int` there needs to be an instance of `gFun`
for `Int`, but first the compiler looks if any instances of kind `*` exist
whatsoever. Since this is not the case, the error is thrown. When an instance
of kind `*` is added (e.g. `gFun{|Bool|} = True`), the error changes to
something more helpful, although the real fix is of course to provide an
instance for `Int` (e.g. `gFun{|Int|} = 0`).
