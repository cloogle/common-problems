# Overloading error no instance available of type

Authors: Peter Achten and Camil Staps

This error occurs when an overloaded function or operator name is used of a specific
type, but the instance can not be found in the program or any one of the imported
modules. The most frequent cause is that you forgot to `import StdEnv` at the top of
your program, immediately below the `module` header. Another cause is that you 
accidentally applied an overloaded function or operator to an argument for which no
instance is available. 

When the class for which no instance can be found ends in `_s`, `_ss`, etc., it
means that there are missing derivations for a generic function (see the second
example below).

## Solutions

- If you forgot to `import StdEnv`, add this line in your module.

- If an instance really does not exist, you need to create one yourself. 

## Examples

```clean
import StdEnv

:: Blood = A | B | AB | O

Start = toString A
```

Here, a new type (`Blood`) is defined. The overloaded function `toString` can be found
in `StdEnv`, but there is no instance of type `Blood` for this function. You need to add
one, for instance like so:

```clean
instance toString Blood
   where toString A  = "A"
         toString B  = "B"
         toString AB = "AB"
         toString O  = "O"
```

---

```clean
import StdGeneric

generic gFun a :: a
gFun{|Bool|} = True

Start :: Int
Start = gFun{|*|}
```

Here, `gFun` has no instance for `Int`. When added, the error is resolved:

```clean
gFun{|Int|} = 0
```
