# Parse error

Authors: Camil Staps

Parse errors usually indicate that the code is ill-formatted. The error message
gives a location in the program code where the structure appears to be wrong.
For instance, `Parse error [Text.icl,12;7]` indicates that there an error is
detected near line 12 of `Text.icl` in column 7. (Depending on the nature of
the error, the actual problem may be somewhere else near this location.)

## Solutions

- Determine what kind of language element the error points at (e.g. is it a
  function definition, or a type definition, or a type class), and check the
  language report to see how these language elements should be structured. 

## Examples

N/A
