# (pattern variable) already defined

Authors: Peter Achten

This error occurs when you have defined a pattern-match that uses the same
variable more than once. 

## Solutions

- Make sure that all pattern variables within the same pattern match have different names.

## Examples

```clean
equal x x = True
```

Here you fix the problem by changing one of the `x` pattern variables into something else, `y` say.

---

```clean
insertAt :: Int a [a] -> [a]
insertAt 0 x ys     = [x:ys]
insertAt _ x []     = [x]
insertAt n x [x:ys] = [x : insertAt (n-1) x ys]  // the error occurs at the left-hand side
//         ^  ^        ^                  ^
//         |  |        |                  |
// x defined twice     so it is unclear which x you mean
               
```

Here you fix the problem by changing one of the `x` pattern variables into something else, `y` say:

```clean
insertAt :: Int a [a] -> [a]
insertAt 0 x ys     = [x:ys]
insertAt _ x []     = [x]
insertAt n x [y:ys] = [y : insertAt (n-1) x ys]
```
