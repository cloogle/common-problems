# result type of dynamic must be non-unique

Authors: Camil Staps

This error occurs when one tries to pack a unique value in a dynamic or unpack
a unique value from a dynamic. Because dynamics are not unique themselves, they
cannot hold unique values, which would break uniqueness propagation.

## Solutions

- There is no general solution to this problem. You need to find another way to
  do what you want to do: either without dynamics, or without uniqueness, but
  you cannot combine the two.

## Examples

```clean
fromDynamic :: Dynamic -> .a
fromDynamic (x :: .a^) = x
fromDynamic _          = abort "type error\n"
```

Here one attempts to unpack a possibly unique value from a dynamic. This is not
allowed. A less useful function that is allowed works only for non-unique
types:

```clean
fromDynamic :: Dynamic -> a
fromDynamic (x :: a^) = x
fromDynamic _         = abort "type error\n"
```

---

An example to explain why dynamics cannot hold unique values:

```clean
fromDynamic :: Dynamic -> .a
fromDynamic (x :: .a^) = x
fromDynamic _          = abort "type error\n"

shareFile :: *File -> (*File, *File)
shareFile f = (fromDynamic d, fromDynamic d)
where
	d :: Dynamic
	d = dynamic f :: *File
```

If this were allowed, `shareFile` would be able to share a unique file, thus
breaking the uniqueness system. This would work by duplicating the value when
it is packed in the non-unique dynamic.
