# selector not defined

Authors: Peter Achten

This error occurs when you are using a non-existing name as a record field selector. 

## Solutions

- Make sure that you have correctly spelled the name (case sensitive).

## Examples

```clean
:: Name = { given :: String, family :: String }

given_name :: Name -> String
given_name name = name.give    // give is not the correct name, it should be given
```
Here you fix the problem by changing `give` into `given`.
