# Unable to get short path name

Authors: Camil Staps

On Windows, compilation of any program may fail when your project code is
located in a directory with a long absolute path to the root of the disk
(e.g., `C:\Documents and Settings\Administrator\My Documents\University\2017-2018\Functional Programming\Course programs`).

## Solutions

- Move the project to a directory with a shorter path to the root of
  the disk (e.g., `C:\my-project`).

## Examples

N/A
