# specified member type is incorrect

Authors: Peter Achten

This is actually a small family of error messages that are all related to the way class instances
need to be written. The core message is `specified member type is incorrect`. This is sometimes
followed by an additional message between `(` and `)`. Examples are `( ! before argument no ok)` 
when a strictness annotation (`!`) has been omitted and `( context not ok)` when you forgot to 
also include class constraints in the instance declaration. 

Even though the language allows you to include a type signature immediately before the instance 
implementation, we advise against this. This is not good practice because it is redundant.
You need to copy the type class scheme yourself and substitute the type constructor by hand without 
deviating from the type class scheme (including strictness and uniqueness annotations). 
You can't even use synonym types. 
Because of its redundancy, a change in the type class scheme forces you to alter each and every
instance definition for which you included this redundant type signature. 

## Solutions

- Inspect your type class instance definitions and remove the redundant type definitions.

- If you still insist on adding type signatures, make sure that your type signature uses exactly
  the same strictness and uniqueness annotations. 

## Examples

```clean
:: MyType = A | B

instance toString MyType 
   where toString :: !MyType -> String  // error is that String is used instead of {#Char}
         toString A = "A"
         toString B = "B"
```
The error is that the type `String` is used in the type signature, whereas the type class definition
in module `StdOverloaded.dcl` defines it as:

```clean
class toString a :: !a -> {#Char}
```

---

```clean
:: MyType = A | B

instance toString MyType 
   where toString :: MyType -> {#Char}  // error is the missing ! annotation
         toString A = "A"
         toString B = "B"
```
The error is the missing strictness annotation (`!`) at the `MyType` argument that is demanded by
the type class definition in module `StdOverloaded.dcl`:

```clean
class toString a :: !a -> {#Char}
```

---

```clean
instance toString (a,b)         // error is the missing context restrictions
   where toString :: !(a,b) -> {#Char} | toString a & toString b  // no error here this time
         toString (x,y) = "(" +++ toString x +++ "," +++ toString y +++ ")"
```
In this example, the type signature is actually correct, but still redundant. 
The error is in the instance declaration, in which the context restrictions are missing.
The solution is to eliminate the type signature (adviced practice), and include the 
context restrictions:

```clean
instance toString (a,b) | toString a & toString b
   where toString (x,y) = "(" +++ toString x +++ "," +++ toString y +++ ")"
```
