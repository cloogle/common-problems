# true undefined

Authors: Peter Achten and Camil Staps

You probably used `true` as the boolean constant `True`, which must start with
an upper case `T`.

## Solutions

- Replace `true` with `True`.

## Examples

```clean
is_hello :: String -> Bool
is_hello "hello" = true       // intend True,  but write true
is_hello _       = false      // intend False, but write false
```

Fix this problem by correctly naming `True` and `False`:

```clean
is_hello :: String -> Bool
is_hello "hello" = True
is_hello _       = False
```
