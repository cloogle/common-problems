# type context should contain one or more type variables

Authors: Peter Achten

This error is caused when a function signature has a type class constraint of a concrete type
instead of a type variable. 

## Solutions

- Check that all type class constraints of function signatures have a type variable instead of
  concrete types.

## Examples

Suppose you have written this module:

```clean
module my_program
import StdEnv

:: Gender = Male | Female

instance fromString Gender
where
    fromString "Male" = Male
    fromString "Female" = Female
    fromString str      = abort ("could not parse '" +++ str +++ "' as Gender")

Start = fromString "Male"   // you added this Start rule to test your function
```
Compiling this program results in an error message: `Start rule cannot be overloaded`.
The reason is that it is not specified which instance of the overloaded function `fromString` must
be chosen by the `Start` function.
You might attempt to fix it by giving `Start` the following type signature:

```clean
Start :: Gender | fromString Gender  // this is the signature that causes the problem
Start = fromString "Male"
```
Compiling this program results in the error message `type context should contain one or more type variables`.
The reason that this is wrong is because type class constraints (the stuff after `|` in a type
signature) must refer to type variables instead of concrete types.
In this case, the type class constraint is not needed at all.
It suffices to state that `Start` returns a value of type `Gender`:

```clean
Start :: Gender   // this is the correct signature for this Start function
Start = fromString "Male"
```
