# undefined in implementation module

Authors: Camil Staps

This error indicates that a function (instance, derivation) signature is given
in a definition module (`.dcl`) with no corresponding implementation in the
implementation module (`.icl`).

The definition module describes a module as a black box, giving information
about what is contained. The implementation module provides the actual
implementation of the various elements. These should match in that all elements
in the definition module should have an implementation (types, classes and
macros do not as they do not contain code). Implementation modules can still
define local functions (types, classes, etc.), which are not exported in the
definition module. These elements are not visible outside the module.

## Solutions

- Check for spelling inconsistencies between the two files.
- Remove (or temporarily comment out) the signatures in the definition module.

## Examples

N/A
