# uniqueness specification of instance conflicts with current application

Authors: Camil Staps

This error occurs because uniqueness attributes are not allowed, and in some
cases silently ignored, in type contexts.

For a recent overview of the limitations of the uniqueness type system, see
Erin van der Veen, 2020, *Mutable Collection Types in Shallow Embedded DSLs*,
MSc. Thesis, Radboud University Nijmegen, especially pp. 36-38:
https://www.ru.nl/publish/pages/769526/erin_van_der_veen.pdf

## Solutions

- It seems the error can only be prevented by simplifying the class constraint
  hierarchy. See the example.

## Examples

```clean
class cls a v
where
	f :: *(v *a) -> *a

instance cls (* ?a, * ?b) ?
where
	f ?None = (?None, ?None)
	f (?Just x) = x

g :: (*v *(*v Int, *v Int)) -> *(*v Int, *v Int) | cls (v Int, v Int) v
g x = f x

Start = g (?Just (?Just 1, ?Just 1))
```

It is not exactly clear why the error occurs. However, it can be prevented by
simplifying the class hierarchy. In particular, `Start` may call `f` directly
(instead of through `g`).

One would expect that modifying the class constraint of `g` to
`cls (*v Int, *v Int) v` would solve the problem as well. However, this syntax
is not allowed and yields the error, "`v` attribute not allowed".
