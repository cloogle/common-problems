# universally quantified type variable not allowed here

Authors: Camil Staps

In some places universally quantified type variables are not permitted.

## Solutions

- Remove the universally quantified type variable.

## Examples

```clean
none :: ?(A.a: a)
none = ?None
```

Here, the intention seems to be that `none` is a `?` value of any type.
However, it is not necessary to specify this, since type variables take
universal scope by default. Therefore, the type `none :: ?a` can be used
instead.
