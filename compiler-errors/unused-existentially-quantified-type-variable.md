# unused existentially quantified type variable

Authors: Camil Staps

This message is emitted when a type constructor has an existentially quantified
type variable that is not used in the constructor.

When there is a context on that variable, the message is an error.
When there is no context, the message is a warning.

## Solutions

- Remove the type variable (and the context, if present).
- Add an argument using the type variable to the constructor.

## Examples

```clean
import StdOverloaded

:: T = E.a: T Int
```

Here, the existential quantification over `a` is meaningless. It is not
problematic, so it concerns a warning. The whole quantification, `E.a:`, can
safely be removed.

---


```clean
import StdOverloaded

:: T = E.a: T Int & toString a
```

It is impossible to use this constructor, because it is impossible to tell the
compiler which instance of `toString` should be used as the instantiation of
`a` cannot be derived.

The quantification (`E.a:`) and the context (`& toString a`) can be removed, or
the type variable can be used in the arguments, for example:

```clean
:: T = E.a: T Int a & toString a
```
