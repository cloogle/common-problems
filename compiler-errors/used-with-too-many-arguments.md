# used with too many arguments

Authors: Camil Staps

This error occurs when a constructor is used with too many arguments.

## Solutions

- Check the usage of the constructor in the line referenced in the error.

## Examples

```clean
Start = ?Just 1 2
```

Here, `?Just :: a -> ?a` can accept only one argument, but two are given.
Correct Start rules are `?Just 1` and `?Just 2`, for example.
