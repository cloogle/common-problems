# used with wrong arity (constructor)

Authors: Camil Staps

This error occurs when a constructor in a pattern is used with an incorrect
number of arguments (arity).

## Solutions

- Carefully check the number of parameters of constructors in the pattern
  matches.

## Examples

```clean
fromJust (?Just x y) = (x,y)
```

Here, `fromJust` seems to incorrectly assume that `?Just` has two parameters,
while it only has one. The correct implementation is `fromJust (?Just x) = x`.
