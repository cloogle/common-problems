# used with wrong arity (type)

Authors: Camil Staps

This error occurs when a type is used with an incorrect number of arguments
(arity).

## Solutions

- Carefully check the number of arguments to types.

## Examples

```clean
:: T = T

Start :: T Int
Start = T
```

Here, `T` expects no arguments, but the type of `Start` gives it one. The `Int`
argument should be removed.
