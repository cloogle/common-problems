# wrongly used or not used at all

Authors: Camil Staps

This error occurs when a class member does not use one of the type variables of
the class.

## Solutions

- Make sure that the types of the class members are correct, and that all type
  variables of the class are needed.
- Sometimes a class with multiple members can be split up in several classes,
  some of which have different type variables (see the second example).

## Examples

```clean
class cls a b :: a
```

This gives the error that `b` is not used at all. Either the type is incorrect
and should also use `b` somewhere, or the class can be rewritten as
`class cls a :: a`.

---

```clean
class cls a b
where
	fa :: a
	fb :: b
```

Here, `fa` does not use `b` and `fb` does not use `a`. Hence, both types are
incorrect. One can split this up in two classes and add a meta-class combining
them if necessary:

```clean
class fa a :: a
class fb b :: b

class cls a b | fa a & fb b
```
