# ld: warning: option -s is obsolete

Authors: Camil Staps

This warning is generated by `clm` on recent Mac OS systems, due to an outdated
call to `ld` in the link step. It can be safely ignored.

## Solutions

N/A

## Examples

N/A
