# not all derived strictness information is exported

Authors: Camil Staps

This warning occurs when a module exports functions with a type that contains
less strictness annotations (`!`) than the compiler could derive. This means
that there are functions with arguments which are used in all alternatives, but
are not annotated with `!`.

Adding strictness annotations can greatly improve performance, but is not
necessary for your program to compile.

## Solutions

- Compile with `clm -lset` to see for which functions not all derived
  strictness information has been exported, then edit those types to include
  the right annotations.

## Examples

```clean
length :: [a] -> Int
length [x:xs] = 1 + length xs
length []     = 0
```

Here, all alternatives of `length` use the argument. Hence, it could be strict.
The warning is resolved by changing the type to `![a] -> Int`.
