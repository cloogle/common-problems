# Not enough memory to allocate heap and stack

Authors: Mart Lubbers and Camil Staps

This error occurs when one attempts to run a program with more heap and/or
stack space than the operating system can provide.

It can also occur during compilation, when there is not enough memory available
for the compiler.

## Solutions

- Run the program with less heap and/or stack space.
- If you really need the heap you can enable memory overcommitting on linux.
- If you really need the stack space you can increase the OS stack limit on
  POSIX using `ulimit`.
- On Windows, closing other programs may help free enough space.

## Examples

N/A
