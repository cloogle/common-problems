# Floating point exception

Authors: Mart Lubbers and Camil Staps

This happens when you divide by zero.

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug.

## Solutions

- Make sure you don't divide by zero.

## Examples

```clean
Start = 37 / 0
```
