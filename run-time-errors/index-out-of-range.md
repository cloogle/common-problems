# index out of range

Authors: Mart Lubbers and Camil Staps

This run time error occurs when a program attempts to retrieve an element from
an array by an illegal index. The index is either negative or greater than or
equal to the size of the array.

Note that stack index checking is not enabled on all platforms by default. If a
program is built without stack index checking, it will simply read from memory
(which may result in garbage or a segmentation fault).

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug.

## Solutions

- It is not possible to 'catch' the run time error. Instead, ensure that you
  only get elements from arrays by correct indices, by checking the size
  beforehand.

## Examples

```clean
Start = "test".[42]
```

Here, 42 is well outside the range of indices of `"test"` (only 0 to 3 would be
accepted).
