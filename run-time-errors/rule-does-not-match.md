# Run time error, rule '&#8230;' in module '&#8230;' does not match

Authors: Mart Lubbers and Camil Staps

This happens when you evaluate the undefined part of a partial function.
In particular, this happens when you have a function of which the patterns are
non-exhaustive.

To debug, you can enable stack tracing when compiling, which will give you a
stack trace up to the moment the runtime error occurs.

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug.

See also {{selector does not match}}.

## Solutions

- Make the function total (implement all possible patterns).
- Make sure you call the function only with values for which it is defined.

## Examples

```clean
:: Letter = A | B | C

next :: Letter -> Letter
next A = B
next B = C

Start = next C
```

This yields:

```text
Run time error, rule 'next' in module 'test' does not match
```

The solution is to either implement `next` for `C` or not call it with `C`.

---

```clean
:: Letter = A | B | C

next :: Letter -> Letter
next A = B
next B = C
Next C = A   // accidently used different name for function, the compiler sees this as a new function without a type

Start = next C
```

This yields:

```text
Run time error, rule 'next' in module 'test' does not match
```

The solution is to rename Next into next.
