# Segmentation fault

Authors: Mart Lubbers and Camil Staps

This error should not normally occur. However, it may pop up when your program
uses low-level features (unchecked array indices; linking with C; custom ABC
code).

On some installations, a segmentation fault can be a symptom of a
{{stack overflow}} or a {{heap full}}.

When a program is compiled without stack index checking, an
{{index out or range}} error may masquerade as a segmentation fault.

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug.

## Solutions

- If this occurs in your own program, enable stack tracing, generation of
  descriptors, and exporting of local labels in `clm`, and disable stripping
  the application. This gives more information on the crash when running the
  program in `gdb`.
- If this occurs in the compiler, report the bug.

## Examples

N/A
