# Run time error, selector does not match

Authors: Camil Staps

This happens when a pattern match in a `let` (or `#`) binding fails.

To debug, you can enable stack tracing when compiling, which will give you a
stack trace up to the moment the runtime error occurs.

If this occurs while compiling code, it means that the compiler crashed.
Try to reduce the code to a MWE (minimal working example) and report the
compiler bug.

See also {{rule does not match}}.

## Solutions

- Cover all possible values using a `case` expression or helper function.
- Make sure the binding is only evaluated with the right value.

## Examples

```clean
// For illustration purposes; this is not good code style.
fromJustInt :: (?Int) -> Int
fromJustInt mb = let (?Just x) = mb in x

Start = fromJustInt ?None
```

This yields:

```text
Run time error, selector does not match
```

There are several ways to solve this. Using a `case` expression:

```clean
fromJustInt :: Int (?Int) -> Int
fromJustInt default mb = case mb of
	?Just x -> x
	?None   -> default

Start = fromJustInt 0 ?None
```

Using patterns:

```clean
fromJustInt :: Int (?Int) -> Int
fromJustInt default ?None     = default
fromJustInt _       (?Just x) = x

Start = fromJustInt 0 ?None
```

By checking that the selector will always match:

```clean
import StdMaybe

fromJustInt :: Int (?Int) -> Int
fromJustInt default mb = if (isJust mb) (let (?Just x) = mb in x) default

Start = fromJustInt 0 ?None
```
